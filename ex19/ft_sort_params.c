/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_params.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tnicolas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/06 16:30:23 by tnicolas          #+#    #+#             */
/*   Updated: 2017/11/06 16:40:34 by tnicolas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void		ft_putchar(char c);

void		ft_putstr_nl(char *str)
{
	while (*str)
		ft_putchar(*str++);
	ft_putchar('\n');
}

int			ft_strcmp(char *s1, char *s2)
{
	while (*s1 == *s2 && *s1)
	{
		s1++;
		s2++;
	}
	return ((unsigned char)*s1 - (unsigned char)*s2);
}

int			main(int ac, char **av)
{
	int		i;
	int		j;
	char	*buf;

	if (ac >= 1)
	{
		j = 0;
		while (++j < ac)
		{
			i = 0;
			while (++i + 1 < ac)
			{
				if (ft_strcmp(av[i], av[i + 1]) > 0)
				{
					buf = av[i];
					av[i] = av[i + 1];
					av[i + 1] = buf;
				}
			}
		}
		i = 0;
		while (++i < ac)
			ft_putstr_nl(av[i]);
	}
	return (0);
}
