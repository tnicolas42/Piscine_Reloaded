/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tnicolas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/06 21:45:16 by tnicolas          #+#    #+#             */
/*   Updated: 2017/11/06 22:00:05 by tnicolas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int			ft_iterative_factorial(int nb)
{
	int		i;
	int		ret;

	if (nb < 0)
		return (0);
	ret = 1;
	i = 0;
	while (++i <= nb)
		ret *= i;
	return (ret);
}
