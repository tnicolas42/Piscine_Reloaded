/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_numbers.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tnicolas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/06 14:15:21 by tnicolas          #+#    #+#             */
/*   Updated: 2017/11/06 14:26:27 by tnicolas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void		ft_putchar(char c);

void		ft_print_numbers(void)
{
	static int	i = '0';

	ft_putchar(i++);
	if (i == '9' + 1)
		i = '0';
	else
		ft_print_numbers();
}
