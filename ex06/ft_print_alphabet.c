/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_alphabet.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tnicolas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/06 13:50:25 by tnicolas          #+#    #+#             */
/*   Updated: 2017/11/06 14:25:24 by tnicolas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void		ft_putchar(char c);

void		ft_print_alphabet(void)
{
	static int	i = 'a';

	ft_putchar(i++);
	if (i == 'z' + 1)
		i = 'a';
	else
		ft_print_alphabet();
}
