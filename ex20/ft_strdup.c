/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tnicolas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/06 16:41:46 by tnicolas          #+#    #+#             */
/*   Updated: 2017/11/06 20:56:27 by tnicolas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char		*ft_strdup(char *str)
{
	char	*dup;
	int		len;

	len = -1;
	while (str[++len])
		;
	if (!(dup = (char*)malloc(sizeof(*dup) * (len + 1))))
		return (NULL);
	while (*str)
		*dup++ = *str++;
	*dup = '\0';
	return (dup - len);
}
