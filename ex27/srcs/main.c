/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tnicolas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/06 21:18:10 by tnicolas          #+#    #+#             */
/*   Updated: 2017/11/06 21:22:47 by tnicolas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/uio.h>

void	ft_display_file(char *pathname)
{
	int		fd;
	char	c;

	fd = open(pathname, O_RDONLY);
	while (read(fd, &c, sizeof(c)) > 0)
		write(STDOUT_FILENO, &c, sizeof(c));
	close(fd);
}

int		main(int argc, char **argv)
{
	if (argc < 2)
		write(STDERR_FILENO, "File name missing.\n", 19);
	else if (argc > 2)
		write(STDERR_FILENO, "Too many arguments.\n", 20);
	else
		ft_display_file(argv[1]);
	return (0);
}
