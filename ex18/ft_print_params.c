/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_params.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tnicolas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/06 15:44:23 by tnicolas          #+#    #+#             */
/*   Updated: 2017/11/06 15:57:20 by tnicolas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void		ft_putchar(char c);

void		ft_putstr_nl(char *str)
{
	while (*str)
		ft_putchar(*str++);
	ft_putchar('\n');
}

int			main(int ac, char **av)
{
	int		i;

	if (ac > 1)
	{
		i = 0;
		while (++i < ac)
			ft_putstr_nl(av[i]);
	}
	return (0);
}
