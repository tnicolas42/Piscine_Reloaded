/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tnicolas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/06 18:09:07 by tnicolas          #+#    #+#             */
/*   Updated: 2017/11/06 18:19:39 by tnicolas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int			*ft_range(int min, int max)
{
	int		*ret;
	int		i;

	if (min >= max)
		return (0);
	if (!(ret = (int*)malloc(sizeof(*ret) * (max - min))))
		return (0);
	i = -1;
	while (min + ++i < max)
		ret[i] = min + i;
	return (ret);
}
